const books = [
    { 
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70 
    }, 
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    }, 
    { 
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    }, 
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function arrayViewList(arr) {
    const ulElem = document.getElementById('root');
    ulElem.insertAdjacentHTML('afterbegin', '<ul></ul>');
    const ul = document.querySelector('ul');

    for (let el of arr) {

        if (el.author && el.name && el.price) {
            let li = document.createElement('li');
            ul.append(li);
            li.textContent = el.author + " - " + el.name + " - " + el.price + " грн.";
            console.log(el)
        } 
        
        try {
            if (!el.author) {
                throw new SyntaxError("Відсутній автор");
            }
            if (!el.name) {
                throw new SyntaxError("Відсутня назва");
            }
            if (!el.price) {
                throw new SyntaxError("Відсутня ціна");
            }
        } catch (error) {
            console.log(error);
        }


    }
}

arrayViewList(books);
